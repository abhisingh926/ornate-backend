from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User

class UserAdmin(BaseUserAdmin):
    list_display = ('email', )
    fieldsets = (
        (None, {'fields': (
                    'email', 
                    'password',
                    'first_name',
                    'last_name',
                    'date_joined',
                    'is_active',
                    'organisation',
                    'phone',
                    'role',
                    'is_staff',
                    'groups',
                    'user_permissions',
                    'is_superuser',
                )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email', 
                'password1', 
                'password2',
                'first_name',
                'last_name',
                'is_active',
                'organisation',
                'phone',
                'user_permissions',
                'role',
                'is_staff',
                'is_superuser',
            )}
        ),
    )

    search_fields = ('email',)
    readonly_fields = ('date_joined','groups')
    ordering = ('email',)

admin.site.register(User, UserAdmin)
