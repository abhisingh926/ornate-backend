from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    organisation = models.CharField(max_length=30)
    phone = models.CharField(max_length=16, blank=True)
    is_staff = models.BooleanField(default=False)
    role = models.ForeignKey('master_data.RoleMaster', on_delete=models.SET_NULL, null=True, blank=True, related_name='users')
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()
    
    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def as_json(self):
        json = {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'phone': self.phone,
            'email': self.email,
            'role': self.role.name
        }
        return json


class EmployeeRegion(models.Model):
    employee = models.ForeignKey(User, on_delete=models.CASCADE, related_name='regions')
    region = models.ForeignKey('master_data.Region', on_delete=models.CASCADE, related_name='employees')

    def __str__(self):
        return self.employee + self.region

