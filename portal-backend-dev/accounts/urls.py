from django.urls import path
from django.conf.urls import include, url
from . import views


urlpatterns = [
    path('change-password/', views.ChangePasswordView.as_view()),
    path('profile', views.ProfileView.as_view()),
]
