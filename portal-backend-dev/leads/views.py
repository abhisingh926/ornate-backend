from .models import *
from .serializers import *
from rest_framework import status, views, viewsets, generics, authentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from django.http import JsonResponse
import datetime


class CompanyViewSet(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def get_queryset(self):
        queryset = Company.objects.all().order_by("-created_at")
        query_params = self.request.query_params
        query = query_params.get("query", None)
        owner = query_params.get("owner", None)
        source = query_params.get("source", None)
        start_date = query_params.get("start_date", None)
        end_date = query_params.get("end_date", None)
        region = query_params.get("region", None)
        product_company = query_params.get("product_company", None)
        
        if query:
            queryset = queryset.filter(name__icontains=query)
        if owner:
            queryset = queryset.filter(owner__pk=owner)
        if source:
            queryset = queryset.filter(lead_source__pk=source)
        if start_date:
            start_date = datetime.date(int(start_date[:4]), int(start_date[4:6]), int(start_date[6:8]))
            queryset = queryset.filter(created_at__gte=start_date)
        if end_date:
            end_date = datetime.date(int(end_date[:4]), int(end_date[4:6]), int(end_date[6:8]))
            queryset = queryset.filter(created_at__lte=end_date)
        if region:
            queryset = list(filter(lambda x: x.region.pk==int(region), queryset))
        if product_company:
            queryset = list(filter(lambda x: any([product.product.company.id==int(product_company)
                                                 for client in x.clients.all()
                                                 for lead in client.leads.all() 
                                                 for product in lead.products.all()]), queryset))
        return queryset
    
    def create(self, request):
        # TODO: Don't need to use this. Will use @create in ClientViewSet for both new and old companies
        pass
    
    @action(detail=True)
    def clients(self, request, pk=None):
        serializer = ClientSerializer(Company.objects.get(id=pk).clients.all(), many=True)
        return JsonResponse(serializer.data, safe=False)
    
    @action(detail=True)
    def leads(self, request, pk=None):
        queryset = []
        clients = Company.objects.get(id=pk).clients.all()
        for client in clients:
            for lead in client.leads.all():
                queryset.append(lead)
        serializer = LeadSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)
         
    def post_queryset(self):
        queryset=Company.objects.all()   


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post_queryset(self):
        queryset=Client.objects.all()

class LeadViewSet(viewsets.ModelViewSet):
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.request.query_params.get("detailed", None):
            return DetailedLeadSerializer
        return LeadSerializer
    
    def get_queryset(self):
        queryset = Lead.objects.all().order_by("-created_at")
        query_params = self.request.query_params
        query = query_params.get("query", None)
        owner = query_params.get("owner", None)
        source = query_params.get("source", None)
        start_date = query_params.get("start_date", None)
        end_date = query_params.get("end_date", None)
        region = query_params.get("region", None)
        product_company = query_params.get("product_company", None)
        # Need to add more filter: status, follow up date, closure date, priority (maybe similar add to CompaniesViewSet)
        
        if query:
            queryset = queryset.filter(clients__company__name__icontains=query)
        if owner:
            queryset = queryset.filter(owner__pk=owner)
        if source:
            queryset = queryset.filter(clients__company__lead_source__pk=source)
        if start_date:
            start_date = datetime.date(int(start_date[:4]), int(start_date[4:6]), int(start_date[6:8]))
            queryset = queryset.filter(created_at__gte=start_date)
        if end_date:
            end_date = datetime.date(int(end_date[:4]), int(end_date[4:6]), int(end_date[6:8]))
            queryset = queryset.filter(created_at__lte=end_date)
        if region:
            queryset = list(filter(lambda x: x.clients.company.region.pk==int(region), queryset))
        if product_company:
            queryset = list(filter(lambda x: any([product.product.company.pk==int(product_company) 
                                                for product in x.products.all()]), queryset))
        
        return queryset
       
    def post_queryset(self):
        queryset=Lead.objects.all()    

    def create(self, request):
        # TODO: don't use this. Instead make it with lead activity
        pass

class ActivityViewSet(viewsets.ModelViewSet):
    queryset = LeadActivity.objects.all()
    serializer_class = ActivitySerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset=LeadActivity.objects.all()
    def post_queryset(self):
        queryset=LeadActivity.objects.all()

class CommentsViewSet(viewsets.ModelViewSet):
    queryset = LeadComment.objects.all()
    serializer_class = CommentsSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset=LeadComment.objects.all()
    def post_queryset(self):
        queryset=LeadComment.objects.all()

class ProductsViewSet(viewsets.ModelViewSet):
    queryset = LeadProduct.objects.all()
    serializer_class = ProductsSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset=LeadProduct.objects.all()
    def post_queryset(self):
        queryset=LeadProduct.objects.all()        