from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=150)

    email_primary = models.EmailField(blank=True, null=True)
    email_secondary = models.EmailField(blank=True, null=True)
    phone_primary = models.CharField(max_length=16, blank=True, null=True)
    phone_secondary = models.CharField(max_length=16, blank=True, null=True)
    website = models.CharField(max_length=30, blank=True, null=True)
    gstin = models.CharField(max_length=16, blank=True, null=True)
    pan = models.CharField(max_length=16, blank=True, null=True)
    lead_source = models.ForeignKey('master_data.LeadSourceMaster', on_delete=models.SET_NULL, null=True, related_name='companies')
    owner = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='owned_companies')

    address = models.TextField(max_length=256, blank=True, null=True)
    area = models.CharField(max_length=50, blank=True, null=True)
    pincode = models.IntegerField(blank=True, null=True)
    city = models.ForeignKey('master_data.City', on_delete=models.SET_NULL, null=True)

    created_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='created_companies')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='modified_companies')
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'company'
        verbose_name_plural = 'companies'
        
    def __str__(self):
        return self.name

    @property
    def all_leads(self):
        return [lead for client in self.clients.all() for lead in list(client.leads.all())]
    
    @property
    def region(self):
        return self.city.state.region

    

class Client(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=False, related_name='clients')
    
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30, null=True)
    email_primary = models.EmailField(blank=True, null=True)
    email_secondary = models.EmailField(blank=True, null=True)
    phone_primary = models.CharField(max_length=16, blank=True)
    phone_secondary = models.CharField(max_length=16, blank=True, null=True)
    designation = models.CharField(max_length=30, blank=True, null=True)

    created_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='created_clients')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='modified_clients')
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name
    

class Lead(models.Model):
    clients = models.ForeignKey(Client, on_delete=models.CASCADE, null=False, related_name='leads')
    owner = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='owned_leads')

    expected_closure_date = models.DateField(blank=True)
    recent_products = models.CharField(max_length=50, blank=True, null=True)
    expected_product_size = models.DecimalField(max_digits=9, decimal_places=3, blank=True, null=True)
    expected_product_size_start = models.DecimalField(max_digits=9, decimal_places=3, blank=True, null=True)
    expected_product_size_end = models.DecimalField(max_digits=9, decimal_places=3, blank=True, null=True)
    requirement_description = models.CharField(max_length=200, blank=True)

    created_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='created_leads')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='modified_leads')
    modified_at = models.DateTimeField(auto_now=True)

    @property
    def last_activity(self):
        return self.activities.order_by('-created_at')[0]
    
    @property
    def status(self):
        return self.last_activity.status

    @property
    def priority(self):
        return self.last_activity.priority
    
    @property
    def follow_up_date(self):
        return self.last_activity.follow_up_date


class LeadComment(models.Model):
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE, null=False, related_name='comments')
    comment = models.CharField(max_length=500)
    
    created_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='created_comments')
    created_at = models.DateTimeField(auto_now_add=True)
    

class LeadActivity(models.Model):
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE, null=False, related_name='activities')
    
    activities = models.TextChoices('activities', 'Call Meeting SMS Whatsapp Email Created Transfered')
    activity = models.CharField(max_length=30, choices=activities.choices)
    statuses = models.TextChoices('statuses', 'Contacted Followed Proposal Closed Lost')
    # TODO: Divide lost into various parts for doing lost lead analysis
    status = models.CharField(max_length=30, choices=statuses.choices)
    priorities = models.TextChoices('priorities', ('High Medium Low Dead'))
    priority = models.CharField(max_length=30, choices=priorities.choices)
    remark = models.CharField(max_length=500, blank=True, null=True)
    follow_up_date = models.DateField()

    created_by = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='created_activities')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Lead activity'
        verbose_name_plural = 'Lead activities'
    
    # Create reciever to make this on Lead


class LeadProduct(models.Model):
    # TODO: either make this OneOnField for now or take it back to Lead Class or enforce these from frontend right now
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE, null=False, related_name='products')
    product = models.ForeignKey('master_data.Product', on_delete=models.SET_NULL, null=True, related_name='leads')
    quantity = models.IntegerField(blank=True, null=True)
