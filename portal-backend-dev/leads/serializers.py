from rest_framework import serializers
from .models import *


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class LeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        fields = '__all__'


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = LeadActivity
        fields = '__all__'


class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeadComment
        fields = '__all__'


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeadProduct
        fields = '__all__'


class DetailedLeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['clients'] = ClientSerializer(instance.clients).data
        response['company'] = CompanySerializer(instance.clients.company).data
        response['activity'] = ActivitySerializer(instance.activities.all(), many=True).data
        response['comments'] = CommentsSerializer(instance.comments.all(), many=True).data
        response['products'] = ProductsSerializer(instance.products.all(), many=True).data
        return response
