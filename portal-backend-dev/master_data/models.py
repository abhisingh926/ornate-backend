from django.db import models


class RoleMaster(models.Model):
    name = models.CharField(max_length=30, unique=True)
    # define permission sets here (or start using Django Groups)

    def __str__(self):
        return self.name


class LeadSourceMaster(models.Model):
    source = models.CharField(max_length=30, unique=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.source


class Region(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name
    
    @property
    def companies(self):
        # TODO: add filter here
        pass


class State(models.Model):
    name = models.CharField(max_length=30, unique=True)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True, related_name='states')
    abbreviation = models.CharField(max_length=3, unique=True)

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=30)
    state = models.ForeignKey(State, on_delete=models.SET_NULL, null=True, related_name='cities')

    class Meta:
        verbose_name_plural = "Cities"

    def __str__(self):
        return self.name


class ProductCompany(models.Model):
    name = models.CharField(max_length=30)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Product companies"
    
    def __str__(self):
        return self.name


class Product(models.Model):
    model_name = models.CharField(max_length=30)
    company = models.ForeignKey(ProductCompany, on_delete=models.SET_NULL, null=True, related_name='products')
    is_active = models.BooleanField(default=True)
    product_type = models.CharField(max_length=30, choices=models.TextChoices('product_types', ('Panel Inverter')).choices)

    def __str__(self):
        return self.model_name

    @property
    def description(self):
        if self.product_type == 'Panel':
            return self.panel_description
        if self.product_type == 'Inverter':
            return self.inverter_description


class PanelDescription(models.Model):
    product = models.OneToOneField(Product, on_delete=models.CASCADE, related_name='panel_description')
    size = models.DecimalField(max_digits=5, decimal_places=2) # Wp

    def __str__(self):
        return self.product.model_name


class InverterDescription(models.Model):
    product = models.OneToOneField(Product, on_delete=models.CASCADE, related_name='inverter_description')
    size = models.DecimalField(max_digits=8, decimal_places=3) # kWp

    def __str__(self):
        return self.product.model_name
