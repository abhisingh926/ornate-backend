from django.shortcuts import render
from .models import *
from .serializers import *
from rest_framework import status, views, viewsets, generics, authentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from django.http import JsonResponse
import datetime
from django.http import HttpResponse    
from django.views.generic import View
from .utils import render_to_pdf
from django.template.loader import get_template

    

class QuotationViewSet(viewsets.ModelViewSet):
    serializer_class = QuotationSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def get_queryset(self):
        queryset=Quotation.objects.all()
    def post_queryset(self):
        queryset=Quotation.objects.all()

class Quotation_productViewSet(viewsets.ModelViewSet):
    queryset = Quotation_product.objects.all()
    serializer_class = Quotation_productSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset=Quotation_product.objects.all()
    def post_queryset(self):
        queryset=Quotation_product.objects.all()

class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def get_queryset(self):
        queryset=Invoice.objects.all()
    def post_queryset(self):
        queryset=Invoice.objects.all()


class Invoice_productViewSet(viewsets.ModelViewSet):
    queryset = Invoice_product.objects.all()
    serializer_class = Invoice_productSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset=Invoice_product.objects.all()
    def post_queryset(self):
        queryset=Invoice_product.objects.all()

class BookingViewSet(viewsets.ModelViewSet):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset=Booking.objects.all()
    def post_queryset(self):
        queryset=Booking.objects.all()

class Booking_productViewSet(viewsets.ModelViewSet):
    queryset = Booking_product.objects.all()
    serializer_class = Booking_productSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (IsAuthenticated,)    

    def get_queryset(self):
        queryset=Booking_product.objects.all()
    def post_queryset(self):
        queryset=Booking_product.objects.all()

class GenerateInvoice(View):
    def get(self, request, *args, **kwargs):
        template = get_template('invoice.html')
        data = {

           
            'pi': 'OS-1920-IV-03218',
            'date': '19-Aug-20',
            'seller': 'Ornate Agencies Pvt. Ltd',
            'seller_address_line1': 'G-14, Arunachal Building, ',
            'seller_address_line2': ' Barakhamba Road, Delhi',
            'seller_address_line3': 'Zip Code-110001 ',
            'seller_gstn': '07AAACO2237Q1Z6 ',
            'seller_contact_person': ' Varun Raj',
            'seller_contact_person_no': ' 9953637462',
            'seller_contact_person_email': 'varun.raj@ornatesolar.com',
           

            'buyer': 'Nexug Industries Private Limited ',
            'buyer_address_line1': '43, 1st Floor, ',
            'buyer_address_line2': 'Krishna Market',
            'buyer_address_line3': 'Kalkaji, New Delhi-110019',
             'buyer_gstn': '07AAACO2237Q1Z6 ',
            'buyer_contact_person': ' Varun Raj',
            'buyer_contact_person_no': ' 9953637462',
            'buyer_contact_person_email': 'varun.raj@ornatesolar.com',

            'consignee': 'Nexug Industries Private Limited ',
            'consignee_address_line1': '43, 1st Floor, ',
            'consignee_address_line2': 'Krishna Market',
            'consignee_address_line3': 'Kalkaji, New Delhi-110019',
            'consignee_gstn': '07AAACO2237Q1Z6 ',
            'consignee_contact_person': ' Varun Raj',
            'consignee_contact_person_no': ' 9953637462',
            'consignee_contact_person_email': 'varun.raj@ornatesolar.com',
        }
        html = template.render(data)
        pdf = render_to_pdf('invoice.html',data)
        return HttpResponse(pdf, content_type='application/pdf')

class GenerateQuotation(View):
    def get(self, request, *args, **kwargs):
        template = get_template('panel-quotation.html')
        data = {

           
            'pi': 'OS-1920-IV-03218',
            'date': '19-Aug-20',
            'seller': 'Ornate Agencies Pvt. Ltd',
            'seller_address_line1': 'G-14, Arunachal Building, ',
            'seller_address_line2': ' Barakhamba Road, Delhi',
            'seller_address_line3': 'Zip Code-110001 ',
            'seller_gstn': '07AAACO2237Q1Z6 ',
            'seller_contact_person': ' Varun Raj',
            'seller_contact_person_no': ' 9953637462',
            'seller_contact_person_email': 'varun.raj@ornatesolar.com',
           

            'buyer': 'Nexug Industries Private Limited ',
            'buyer_address_line1': '43, 1st Floor, ',
            'buyer_address_line2': 'Krishna Market',
            'buyer_address_line3': 'Kalkaji, New Delhi-110019',
             'buyer_gstn': '07AAACO2237Q1Z6 ',
            'buyer_contact_person': ' Varun Raj',
            'buyer_contact_person_no': ' 9953637462',
            'buyer_contact_person_email': 'varun.raj@ornatesolar.com',

          
        }
        html = template.render(data)
        pdf = render_to_pdf('panel-quotation.html',data)
        return HttpResponse(pdf, content_type='application/pdf')        