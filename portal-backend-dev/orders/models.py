from accounts import models as account_models
from master_data import models as master_models
from leads import models as lead_models
from django.db import models

# Create your models here.
class Quotation(models.Model):
    date = models.DateField()
    company = models.ForeignKey(lead_models.Company, on_delete=models.CASCADE, null=False, related_name="compnay_quotation")
    client = models.ForeignKey(lead_models.Client, on_delete=models.CASCADE, null=False, related_name="client_quotation")
    
    user = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='user_quotation')
    p_terms = models.CharField(max_length=500)
    d_terms = models.CharField(max_length=500)

    created_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='created_quotation')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='modified_quotation')
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Quotation"
    
    def __str__(self):
            return self.p_terms  

class Quotation_product(models.Model):
    invoice = models.ForeignKey(Quotation, on_delete=models.CASCADE, null=False, related_name="quotation_product")
    product_details = models.ForeignKey(master_models.Product, on_delete=models.SET_NULL, null=True, related_name='product_details_quotation_product')
    product_req = models.IntegerField()
    offer_price  = models.FloatField()
    
    created_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='created_quotation_product')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='modified_quotation_product')
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Quotation_product"
    
    def __str__(self):
            return self.product_req

class Invoice(models.Model):
    date = models.DateField()
    company = models.ForeignKey(lead_models.Company, on_delete=models.CASCADE, null=False, related_name="compnay_invoice")
    client = models.ForeignKey(lead_models.Client, on_delete=models.CASCADE, null=False, related_name="client_invoice")
    
    shipping_address  = models.CharField(max_length=30) # To link to org table soon
    company_seller  = models.CharField(max_length=30) # To link to org table soon
    user = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='user_invoice')
    duties  = models.FloatField()
    freight  = models.FloatField()
    custom_clr  = models.FloatField()
    total_amount  = models.FloatField()
    advance_r  = models.FloatField()
    balance_due  = models.FloatField()
    p_terms = models.CharField(max_length=500)
    d_terms = models.CharField(max_length=500)

    created_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='created_invoice')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='modified_invoice')
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Invoices"
    
    def __str__(self):
            return self.duties  

class Invoice_product(models.Model):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, null=False, related_name="invoice_product")
    product_details = models.ForeignKey(master_models.Product, on_delete=models.SET_NULL, null=True, related_name='product_details_invoice_product')
    product_req = models.IntegerField()
    offer_price  = models.FloatField()
    
    created_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='created_invoice_product')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='modified_invoice_product')
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Invoice_products"
    
    def __str__(self):
            return self.product_req


class Booking(models.Model):
    date = models.DateField()
    company = models.ForeignKey(lead_models.Company, on_delete=models.CASCADE, null=False, related_name="compnay_booking")
    client = models.ForeignKey(lead_models.Client, on_delete=models.CASCADE, null=False, related_name="client_booking")
    
    warehouse  = models.CharField(max_length=30) # To link to org table soon
    remark = models.CharField(max_length=500)
    user = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='user_booking')

    created_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='created_booking')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='modified_booking')
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Bookings"
    
    def __str__(self):
            return self.warehouse  

class Booking_product(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE, null=False, related_name="booking_product")
    product_details = models.ForeignKey(master_models.Product, on_delete=models.SET_NULL, null=True, related_name='product_details_booking_product')
    product_req = models.IntegerField()
    created_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='created_booking_product')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(account_models.User, on_delete=models.SET_NULL, null=True, related_name='modified_booking_product')
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Booking_products"
    
    def __str__(self):
            return self.product_req