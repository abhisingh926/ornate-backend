from rest_framework import serializers
from .models import *


class QuotationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quotation
        fields = '__all__'

class Quotation_productSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quotation_product
        fields = '__all__'


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'


class Invoice_productSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice_product
        fields = '__all__'


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = '__all__'


class Booking_productSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking_product
        fields = '__all__'

