
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework import routers
from rest_framework.schemas import get_schema_view
from accounts.views import *
from leads.views import *
from orders.views import *

router = routers.DefaultRouter()
router.register('users', UserViewSet, basename='users')
router.register('companies', CompanyViewSet, basename='companies')
router.register('clients', ClientViewSet, basename='clients')
router.register('leads', LeadViewSet, basename='leads')
router.register('orders', QuotationViewSet, basename='quotation')



schema_view = get_schema_view(title='Design Studio API')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-token-auth/', CustomAuthToken.as_view()),
    path('api/user/change-password/', ChangePasswordView.as_view()),
    path('schema/', schema_view),
    path('pdf/', GenerateInvoice.as_view()),
    path('quotation/', GenerateQuotation.as_view()),
]
